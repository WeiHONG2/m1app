package com.example.renan.m1project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.renan.m1project.adapter.MusicListAdapter;
import com.example.renan.m1project.factory.MusicListItem;
import com.example.renan.m1project.provider.MusicInfoProvider;

import java.util.ArrayList;
import java.util.List;

public class MusicListActivity extends AppCompatActivity {

    public RecyclerView recyclerView;
    public MusicListAdapter adapter;
    public List<MusicListItem> musicList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        musicList = (new MusicInfoProvider()).getMusicList();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        recyclerView = findViewById(R.id.music_list);
        adapter = new MusicListAdapter(this, musicList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public void onClick(View view) {
        int songID = 1;
        Intent intent = new Intent(this, SongPlayingActivity.class);
        intent.putExtra("songID", songID);
        startActivity(intent);
    }

    public void onPress(View view) {

    }
}
