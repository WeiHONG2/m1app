package com.example.renan.m1project;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class LightActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
    SeekBar sb_ld,sb_hyd;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        openAleterWindow();
        setContentView(R.layout.light);
        permission();
        initView();
        initListner();
//        sb_ld.setOnSeekBarChangeListener(this);
//        sb_hyd.setOnSeekBarChangeListener(this);
    }

    private void initListner() {
        Button bt_test=findViewById(R.id.bt_test);
        bt_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAleterWindow();
            }
        });
    }


    private void openAleterWindow() {
//        WindowManager mWindowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
//        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
////        int flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
////        params.flags = flags;
//
//        params.format = PixelFormat.TRANSLUCENT;
//
//        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.MATCH_PARENT;
//        params.gravity = Gravity.CENTER;
//        View view=LayoutInflater.from(this).inflate(R.layout.dailog,null);
//        mWindowManager.addView(view,params);
        dialog=new Dialog(this,R.style.dialog_translucent);
        dialog.setContentView(R.layout.dialog);
        dialog.show();

        SeekBar sb_ld=dialog.findViewById(R.id.sb_ld);
        sb_ld.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ImageView iv=dialog.findViewById(R.id.ll_main);
                iv.setBackgroundColor(NoColor(100-progress));

//               changeAppBrightness(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        SeekBar sb_r=dialog.findViewById(R.id.sb_r);
        sb_r.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

//                View view=LayoutInflater.from(ReadActivity.this).inflate(R.layout.dailog,null);
//                view.setBackgroundColor(Color.argb(1,255,0,0));
//                dialog.setContentView(view);
                ImageView iv=dialog.findViewById(R.id.ll_main);
                iv.setBackgroundColor(RedColor(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        SeekBar sb_b=dialog.findViewById(R.id.sb_b);
        sb_b.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

//                View view=LayoutInflater.from(ReadActivity.this).inflate(R.layout.dailog,null);
//                view.setBackgroundColor(Color.argb(1,255,0,0));
//                dialog.setContentView(view);
                ImageView iv=dialog.findViewById(R.id.ll_main);
                iv.setBackgroundColor(BlueColor(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        SeekBar sb_g=dialog.findViewById(R.id.sb_g);
        sb_g.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

//                View view=LayoutInflater.from(ReadActivity.this).inflate(R.layout.dailog,null);
//                view.setBackgroundColor(Color.argb(1,255,0,0));
//                dialog.setContentView(view);
                ImageView iv=dialog.findViewById(R.id.ll_main);
                iv.setBackgroundColor(GreenColor(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });




    }

    private void initView() {
//        sb_ld=findViewById(R.id.sb_ld);
//        sb_hyd=findViewById(R.id.sb_hyd);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Log.e("this",seekBar.equals(sb_ld)+":");
        Log.e("this",progress+"::");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    public void permission(){
        if (Build.VERSION.SDK_INT >= 23) {
            if(!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                startActivity(intent);
            }
        }
    }


    /**

     * 过滤蓝光

     * @param blueFilterPercent 蓝光过滤比例[10-80]

     * */

    public int BlueColor(int blueFilterPercent)

    {
        int realFilter = blueFilterPercent;
        if (realFilter < 10)
        {
            realFilter = 10;
        }
        else if (realFilter > 80)
        {
            realFilter = 80;
        }
        int a = (int) (realFilter / 80f * 180);
        int r = (int) (200 - (realFilter / 80f) * 190);
        int g = (int) (180 - ( realFilter / 80f) * 170);
        int b = (int) (60 - realFilter / 80f * 60);
        return Color.argb(a, r, g, b);
    }

    public int RedColor(int blueFilterPercent)
    {
        int realFilter = blueFilterPercent;
        if (realFilter < 10)
        {
            realFilter = 10;
        }
        else if (realFilter > 80)
        {
            realFilter = 80;
        }
        int a = (int) (realFilter / 80f * 180);
        int b= (int) (200 - (realFilter / 80f) * 190);
        int g = (int) (180 - ( realFilter / 80f) * 170);
        int r = (int) (60 - realFilter / 80f * 60);
        return Color.argb(a, r, g, b);
    }

    public int GreenColor(int blueFilterPercent)
    {
        int realFilter = blueFilterPercent;
        if (realFilter < 10)
        {
            realFilter = 10;
        }
        else if (realFilter > 80)
        {
            realFilter = 80;
        }
        int a = (int) (realFilter / 80f * 180);
        int b= (int) (200 - (realFilter / 80f) * 190);
        int r= (int) (180 - ( realFilter / 80f) * 170);
        int g = (int) (60 - realFilter / 80f * 60);
        return Color.argb(a, r, g, b);

    }

    public int NoColor(int blueFilterPercent)
    {
        int realFilter = blueFilterPercent;
        if (realFilter < 10)
        {
            realFilter = 10;
        }
        else if (realFilter > 80)
        {
            realFilter = 80;
        }
        int a = (int) (realFilter / 80f * 180);
        int b= (int) (60 - realFilter / 80f * 60);
        int r= (int) (60 - realFilter / 80f * 60);
        int g = (int) (60 - realFilter / 80f * 60);
        return Color.argb(a, r, g, b);

    }

    public void changeAppBrightness(int brightness) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();

        lp.screenBrightness = Float.valueOf(brightness) * (1f / 255f);

        getWindow().setAttributes(lp);
    }




}
