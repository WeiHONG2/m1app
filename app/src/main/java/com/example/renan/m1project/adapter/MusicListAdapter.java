package com.example.renan.m1project.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.renan.m1project.R;
import com.example.renan.m1project.factory.MusicListItem;

import java.util.List;

public class MusicListAdapter extends RecyclerView.Adapter<MusicListAdapter.MyHolder> {
    Context context;
    List<MusicListItem> musicList;
    public MusicListAdapter(Context context, List<MusicListItem> musicList){
        this.context = context;
        this.musicList = musicList;
    }
    public void update(List<MusicListItem> musicList){
        this.musicList = musicList;
    }
    class MyHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public MyHolder(View itemView){
            super(itemView);
            textView = itemView.findViewById(R.id.test);
        }
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_list_item, parent, false);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position){
        String a = "123";
        MusicListItem musicListItem = musicList.get(position);
        holder.textView.setText(musicListItem.toString());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO play selected music
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }
}
