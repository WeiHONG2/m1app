package com.example.renan.m1project.provider;

import android.os.Environment;

import com.example.renan.m1project.factory.MusicListItem;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class MusicInfoProvider {

    private List<MusicListItem> musicList;
    private String jsonFilePath;

    public MusicInfoProvider(){
        String urlText = "http://localhost:8080/java/songs";
        musicList = new ArrayList<>();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        jsonFilePath = path + "/music_list.json";
        getMusicInfoFromURL(urlText);
    }

    public void getMusicInfoFromURL(String urlText) {
        //get json file from URL which include music info
        File music_list_json = new File(jsonFilePath);
        if(!music_list_json.exists()) {
            try {
                music_list_json.createNewFile();
                FileWriter writer = new FileWriter(music_list_json);
                URL url = new URL(urlText);
                URLConnection uc = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    writer.write(inputLine);
                    System.out.println(inputLine);
                }
                in.close();
                writer.flush();
                writer.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        getMusicInfoFromJson();
    }

    public void getMusicInfoFromJson(){
        JsonParser parse = new JsonParser();
        try{
            JsonObject jsonMusic = (JsonObject)parse.parse(new FileReader(jsonFilePath));
            JsonArray jsonArray = jsonMusic.getAsJsonArray();
            for(int i=0;i<jsonArray.size();i++){
                JsonObject json = jsonArray.get(i).getAsJsonObject();
                String name = json.get("name").getAsString();
                String artist = json.get("artist").getAsString();
                String year = json.get("year").getAsString();
                String length = json.get("length").getAsString();
                String uri = json.get("uri").getAsString();
                musicList.add(new MusicListItem(name,artist,year,length,uri));
            }
        }catch(JsonIOException e){
            e.printStackTrace();
        }catch(JsonSyntaxException e){
            e.printStackTrace();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IllegalStateException e){
            e.printStackTrace();
        }
    }

    public List<MusicListItem> getMusicList() {
        return musicList;
    }
}
