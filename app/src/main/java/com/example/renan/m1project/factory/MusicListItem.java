package com.example.renan.m1project.factory;

public class MusicListItem {
    private String musicName;
    private String musicArtist;
    private String publishYear;
    private String musicLength;
    private String musicURI;

    public MusicListItem(String musicName, String musicArtist, String publishYear, String musicLength, String musicURI){
        this.musicName = musicName;
        this.musicArtist = musicArtist;
        this.publishYear = publishYear;
        this.musicLength = musicLength;
        this.musicURI = musicURI;
    }

    public String getMusicName() {
        return musicName;
    }

    public String getMusicArtist() {
        return musicArtist;
    }

    public String getPublishYear() {
        return publishYear;
    }

    public String getMusicLength() {
        return musicLength;
    }

    public String getMusicURI() {
        return musicURI;
    }

    @Override
    public String toString() {
        return "MusicListItem{" +
                "musicName='" + musicName + '\'' +
                ", musicArtist='" + musicArtist + '\'' +
                ", publishYear='" + publishYear + '\'' +
                ", musicLength='" + musicLength + '\'' +
                ", musicURI='" + musicURI + '\'' +
                '}';
    }
}
